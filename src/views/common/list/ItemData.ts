export default class ItemData {
    public active: boolean = false;
    public key: string = '';
    public name: string = '';
    public avatar: string = 'assets/images/common/head/user/1.png';
    public gray: boolean = true;
    public red: boolean = false;
    public redCount: number = 0;
}
